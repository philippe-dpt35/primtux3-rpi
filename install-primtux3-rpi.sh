#!/bin/bash

########################################
# Script de construction d'une PrimTux #
# sur Raspberry Pi disposant d'une     #
# Raspbian lite installée.             #
# Auteur: Philippe Ronflette           #
# philippe.dpt35@yahoo.fr              #
########################################

debut=$(date +%s)

# Paramétrage des sources, à modifier si nécessaire
liensource="https://framagit.org/Steph/primtux3/-/archive/master/primtux3-master.tar"
archive="primtux3-master.tar"
config_chromium="https://www.primtux.fr/Documentation/armhf/chromium.tar.gz"
config_panel="http://www.primtux.fr/Documentation/armhf/xfce4.tar.gz"
config_pcmanfm="https://www.primtux.fr/Documentation/armhf/pcmanfm.conf.tar"
config_libfm="https://www.primtux.fr/Documentation/armhf/libfm.conf.tar"
icone_gspeech="https://www.primtux.fr/Documentation/armhf/gspeech.png"

version="PrimTux4 Debian9 RPi DG"

dossiersource=$(echo "$archive" | sed 's/\.tar//')

# Listes des paquets à installer
paquets_base="xorg fluxbox lightdm lxappearance lxpanel xfce4-panel roxterm leafpad spacefm pcmanfm menulibre rox-filer gvfs samba synaptic gdebi firefox-esr"
paquets_impression="cups cups-pdf hplip hplip-gui hpijs-ppds printer-driver-c2050 printer-driver-c2esp printer-driver-cjet printer-driver-escpr openprinting-ppds gutenprint-locales printer-driver-gutenprint printer-driver-hpcups printer-driver-postscript-hp printer-driver-m2300w printer-driver-min12xxw printer-driver-pnm2ppa printer-driver-ptouch printer-driver-sag-gdi printer-driver-splix printer-driver-foo2zjs printer-driver-hpijs printer-driver-pxljr system-config-printer magicfilter djtools librecode0 recode lpr lsb-compat"
paquets_polices="xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus fonts-droid-fallback fonts-liberation gnome-font-viewer fonts-opendyslexic fonts-dejavu-core fonts-dejavu-extra fonts-liberation fonts-sil-gentium fonts-sil-gentium-basic gnome-icon-theme-symbolic ttf-dejavu-core fonts-wine"
paquets_themes="gtk2-engines-murrine murrine-themes adwaita-icon-theme faenza-icon-theme gtk2-engines gtk2-engines-pixbuf"
paquets_wifi="blueman bluetooth wicd gnome-bluetooth"
paquets_divers="python-glade2 numlockx zenity dansguardian baobab xsane audacity libreoffice shutter xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-fsguard-plugin xfce4-indicator-plugin xfce4-mixer xfce4-netload-plugin xfce4-quicklauncher-plugin xfce4-systemload-plugin xfce4-volumed file-roller gparted filezilla xpdf osmo xournal qalculate-gtk arandr openshot soundconverter winff gnome-mplayer hardinfo htop libreoffice-help-fr libreoffice-l10n-fr libreoffice-pdfimport dillo disk-manager synapse tcl tk8.5 tk8.6 tinyproxy accountsservice mirage pinta gigolo anacron childsplay-alphabet-sounds-fr cpulimit dkms dnsutils fonts-sil-andika frozen-bubble gcompris gcompris-sound-fr gimp-plugin-registry gnome-system-tools gnucap goldendict gnupg2 gtans gxmessage hannah jmtpfs katepart klettres ktuberling mlocate monsterz mtp-tools pavucontrol pysycache pysycache-buttons-beerabbit pysycache-buttons-crapaud pysycache-buttons-wolf pysycache-click-sea pysycache-dblclick-butterfly pysycache-move-food pysycache-move-plants pysycache-move-sky pysycache-move-sports pysycache-puzzle-photos ri-li scratch seahorse seahorse-adventures stellarium supertux tuxpaint tuxmath trash-cli xscreensaver libsexy2 libtk-img iwidgets4 python-webkit childsplay chromium-browser firefox-esr-l10n-fr jclic powermgmt-base pm-utils udevil user-setup whois vim-runtime busybox fusesmb blobby tcl8.4 tcl8.5 alsa-tools vlc pulseaudio gstreamer0.10-tools gstreamer1.0-fluendo-mp3 gstreamer0.10-pulseaudio gstreamer1.0-packagekit gstreamer1.0-plugins-ugly gstreamer0.10-plugins-good gstreamer0.10-plugins-base fotowall gtk3-engines-unico musescore openjdk-8-jre libttspico-utils gnome-calculator xfburn gspeech"
paquets_primtux="accueil-primtux2 administration-eleves-primtux arreter-primtux autologin-primtux-ubuntu documentation-primtux drgeo-raspbian edit-interactive-svg fskbsetting geonext-primtux geotortue-stretch geotortue-stretch gtkdialog handymenu intef-exe jnavigue-primtux lampp-primtux-rpi leterrier-aller leterrier-calculette-capricieuse leterrier-calculment leterrier-cibler leterrier-imageo leterrier-fubuki leterrier-suitearithmetique leterrier-tierce lightdm-webkit-greeter microscope-virtual-primtux mothsart-wallpapers-primtux multiplication-station-primtux omnitux-light proxy-protect-firefox-esr pylote-primtux pysiogame python-sexy qdictionnaire sauve-carte tbo toutenclic wordsearchcreator openboard"

# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/install-primtux-rpi.log"
if ! [ -e "$fichierlog" ]
	then > "$fichierlog"
fi
date >> "$fichierlog"
exec 2>>"$fichierlog"

# Ajout des dépôts PrimTux
if ! grep -e "^deb http://depot.primtux.fr/repo/debs/ PrimTux-armhf main" /etc/apt/sources.list >/dev/null
   then echo "deb http://depot.primtux.fr/repo/debs/ PrimTux-armhf main" >> /etc/apt/sources.list
fi

# Ajout de la clé publique du dépôt
wget -O - http://depot.primtux.fr/repo/debs/key/PrimTux.gpg.key | apt-key add -
apt-get update
apt-get dist-upgrade -y

# Paquets de base
for paquet in ${paquets_base}
do
   apt-get install -y "$paquet"
done

# Paquets de la distribution PrimTux
for paquet in ${paquets_impression}
do
   apt-get install -y "$paquet"
done
for paquet in ${paquets_polices}
do
   apt-get install -y "$paquet"
done
for paquet in ${paquets_themes}
do
   apt-get install -y "$paquet"
done
for paquet in ${paquets_wifi}
do
   apt-get install -y "$paquet"
done
for paquet in ${paquets_divers}
do
   apt-get install -y "$paquet"
done

#Pour résoudre les problèmes avec lightdm
apt-get install -y libwebkitgtk-3.0-0

# Installation des paquets du dépôt PrimTux
for paquet in ${paquets_primtux}
do
   apt-get install -y "$paquet"
done

# Pour le WiFi
apt-get remove -y dhcpcd5
apt-get purge -y network-manager network-manager-gnome
apt-get autoremove -y

apt-get --fix-broken install -y

# Installation de log2ram
wget https://github.com/azlux/log2ram/archive/master.tar.gz
tar xf master.tar.gz
rm master.tar.gz
cd log2ram-master
chmod +x install.sh
./install.sh
cd ..
rm -r log2ram-master

# Récupération des sources de PrimTux 3
mkdir /tmp/sources-primtux
wget -P /tmp/sources-primtux "$liensource"
tar xvf /tmp/sources-primtux/"$archive" -C /tmp/sources-primtux
rm /tmp/sources-primtux/"$archive"

# Copie les fichiers sur le système
rsync -av /tmp/sources-primtux/"$dossiersource"/config/includes.chroot/* /
# Remplace les fichiers de configuration du gestionnaire de fichiers par ceux pour RPi
wget -P /tmp "$config_pcmanfm"
wget -P /tmp "$config_libfm"
tar xvf /tmp/pcmanfm.conf.tar -C /etc/skel-mini/.config/pcmanfm/default
tar xvf /tmp/libfm.conf.tar -C /etc/skel-mini/.config/libfm
tar xvf /tmp/pcmanfm.conf.tar -C /etc/skel-super/.config/pcmanfm/default
tar xvf /tmp/libfm.conf.tar -C /etc/skel-super/.config/libfm
tar xvf /tmp/pcmanfm.conf.tar -C /etc/skel-maxi/.config/pcmanfm/default
tar xvf /tmp/libfm.conf.tar -C /etc/skel-maxi/.config/libfm
rm /tmp/pcmanfm.conf.tar
rm /tmp/libfm.conf.tar

# Suppression de l'utilisateur pi
userdel -r pi

# Applications des scripts de configuration de PrimTux
sh /tmp/sources-primtux/"$dossiersource"/config/hooks/normal/1000.useradd.hook.chroot
# Copie les répertoires de chaque utilisateur
rsync -av /etc/skel/ /home/administrateur
rsync -av /etc/skel-mini/ /home/01-mini
rsync -av /etc/skel-super/ /home/02-super
rsync -av /etc/skel-maxi/ /home/03-maxi

# on donne à chaque répertoire utilisateur ses droits
chown -R 01-mini:01-mini /home/01-mini
chown -R 02-super:02-super /home/02-super
chown -R 03-maxi:03-maxi /home/03-maxi
chown -R administrateur:administrateur /home/administrateur

sh /tmp/sources-primtux/"$dossiersource"/config/hooks/normal/1001.chown-rep.hook.chroot
sh /tmp/sources-primtux/"$dossiersource"/config/hooks/normal/1003.bash-false.hook.chroot
sh /tmp/sources-primtux/"$dossiersource"/config/hooks/normal/1004.rep-public.hook.chroot
sh /tmp/sources-primtux/"$dossiersource"/config/hooks/normal/1005.scs.hook.chroot

# Suppression des répertoires de calcul@TICE pour l'instant non intégré 
rm -rf /home/{01-mini,02-super,03-maxi,administrateur}/clc-linux

# On nettoie !
rm -rf /tmp/sources-primtux

# Paramètres de chromium qui remplace seamonkey
wget -P /home/administrateur/.config "$config_chromium"
tar xvf /home/administrateur/.config/chromium.tar.gz -C /home/administrateur/.config
rm /home/administrateur/.config/chromium.tar.gz

# Configuration du panel avec Chromium au lieu de Seamonkey
wget -P /home/administrateur/.config "$config_panel"
tar xvf /home/administrateur/.config/xfce4.tar.gz -C /home/administrateur/.config
rm /home/administrateur/.config/xfce4.tar.gz
# suppression du dossier seamonkey
rm -Rf /home/{01-mini,02-super,03-maxi,administrateur}/seamonkey
# Correction du nom différent pour l'application calcul mental
ln -s /usr/bin/leterrier-calculment /usr/bin/leterrier-calcul-mental
# Icone Gspeech
wget "$icone_gspeech"
cp gspeech.png /usr/share/pixmaps/gspeech.png

# Vérification des paquets installés
paquets=$(echo "$paquets_base $paquets_impression $paquets_polices $paquets_themes $paquets_wifi $paquets_divers $paquets_primtux")
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > /tmp/installes.txt
echo "Paquets manquants :" >> "$fichierlog"
for paquet in ${paquets}
do
   if ! grep "$paquet" /tmp/installes.txt > /dev/null; then
      echo "$paquet" >> "$fichierlog"
   fi
done
rm /tmp/installes.txt

# Indication de version de l'OS
echo "$version" >/etc/primtux_version

# Nettoyage du fichier des erreurs
sed -i '/[1-9].*K \.\.\.\.*/ d' "$fichierlog"
fin=$(date +%s)
temps=$(($fin-$debut))
temps=$(echo $temps |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "
Les opérations sont terminées et ont duré $temps
Un fichier des erreurs a été créé en /var/log/install-primtux-rpi.log
Veuillez redémarrer le système."
echo "
#######################################################################
" >> "$fichierlog"
exit 0
