**Script de construction de PrimTux4 Debian9 RPi DG sur Raspberry Pi**

Ce script permet de construire une distribution PrimTux4 Debian9 RPi DG (anciennement PrimTux3) pour le nano ordinateur Raspberry Pi sur la base d'une Raspbian lite.

*Nota : depuis novembre 2018, les différentes versions de Primtux ont été harmonisées et ont été rebaptisées. PrimTux3 devient ainsi PrimTux4 Debian9 RPi DG.*
 
## Utilisation :

Le Raspberry Pi doit être démarré avec une carte SD sur laquelle est installée une Raspbian Lite stretch.

Connectez-vous avec le login par défaut:

```
pi
```

Mot de passe par défaut de Raspbian:

```
raspberry
```

ATTENTION: par défaut Raspbian est configuré avec un clavier anglo-saxon. Il faudra en tenir compte lors des saisies. Pour le mot de passe, avec un clavier AZERTY, il faut saisir `rqspberry`.

Configurez les paramètres de localisation, de clavier, de WiFi à l'aide de l'utilitaire inclus raspi-config

```
sudo raspi-config
```

(sudo `rqspi)config` avec un clavier AZERTY)

Activez le compte root en lui attribuant un mot de passe:

```
sudo passwd root
```

Je vous invite à saisir `tuxprof` comme mot de passe, car c'est celui proposé par défaut dans PrimTux. Vous pourrez toujours le changer par la suite.

Redémarrez puis connectez-vous sous le compte root.

```
wget http://www.primtux.fr/Documentation/armhf/install-primtux3-rpi.sh

chmod +x install-primtux3-rpi.sh

./install-primtux3-rpi.sh
```

L'opération nécessite plusieurs heures, et dépend de la qualité de votre liaison Internet.

Redémarrez en fin d'opération.

Un [tutoriel détaillé](https://framagit.org/philippe-dpt35/primtux3-rpi/wikis/Tutoriel-de-construction-de-primTux3-sur-Raspberry-Pi) est consultable sur le wiki.

Le script crée un fichier log des erreurs en `/var/log/install-primtux-rpi.log`
